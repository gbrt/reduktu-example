async function partialTemplate(partial) {
    const param1 = r.params.dynamic_content_example.exampleParam1;
    const param2 = r.params.dynamic_content_example.exampleParam2;
    
    partial.querySelector('h3').innerHTML = param1;
    
    let ul = partial.querySelector('ul');
    
    for (let i = 1; i < param2; i++) {
        let li = document.createElement("li");
        
        li.innerHTML = i;
        ul.appendChild(li);
    }
    
    return partial;
}
